import initialState from "./initialState";
import {
    IS_ERROR,
    IS_LOADING,
    FETCH_PROPERTIES,
    RECEIVE_PROPERTIES,
    SET_FILTERS
} from "../actions/allActions";

export default function appRed(state = initialState, action) {
    switch (action.type) {
        case FETCH_PROPERTIES:
            return {...state, loading: true};
        case RECEIVE_PROPERTIES:
            return {...state, properties: action.data, error: false, loading: false};
        case IS_ERROR:
            return {...state, error: action.error, loading: false};
        case IS_LOADING:
            return {...state, loading: action.loading};
        case SET_FILTERS:
            return {...state, filters: action.filters};
        default:
            return state;
    }
}