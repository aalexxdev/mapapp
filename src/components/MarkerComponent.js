import React from 'react'
import {Marker, Popup} from 'react-leaflet'
import Toast from 'react-bootstrap/Toast';
import ListGroup from 'react-bootstrap/ListGroup';

export default function MarkerComponent(props) {
    if (!props || !props.property) return null;
    return <Marker position={props.property.Coordinates}>
        <Popup>
            <Toast>
                <Toast.Header closeButton={false}>
                    <strong className="mr-auto">Property</strong>
                    <small>Price: {props.property['Price/m^2']} chf</small>
                </Toast.Header>
                <Toast.Body>
                    <ListGroup variant="flush">
                        <ListGroup.Item> BuildingType: {props.property.BuildingType} </ListGroup.Item>
                        <ListGroup.Item> Parking: {props.property.Parking.toLowerCase() === "" ? "No" : "Yes"}</ListGroup.Item>
                    </ListGroup>
                </Toast.Body>
            </Toast>
        </Popup>
    </Marker>
}