import React, {Suspense} from 'react'
import {Map, TileLayer} from 'react-leaflet'

const MarkerComponent = React.lazy(() => import('./MarkerComponent'));
const position = [47.4041114, 8.5558533]
export const MapComponent = (props) => {
    if (Object.keys(props.filters).length > 0) {
        const filtered = props.properties.filter((pr) => {
            return (parseFloat(pr['Price/m^2']) <= parseFloat(props.filters['Price/m^2']) &&
                props.filters.buildingType[pr.BuildingType] &&
                props.filters.parking[pr.Parking]
            )
        })
        return <Map center={position} zoom={13} style={{"height": "65vh", "width": "100hw"}}>
            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
            />
            <Suspense fallback={<div>Loading...</div>}>
                {filtered.map((item, i) => {
                    return <MarkerComponent key={i} property={item}/>
                })}
            </Suspense>
        </Map>
    } else return null;
}