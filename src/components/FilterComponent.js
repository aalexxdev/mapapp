import React from 'react';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Navbar from "react-bootstrap/Navbar";

export default function FilterComponent(props) {

    if (Object.keys(props.filters).length <= 0) {
        return null
    } else {
        return <Navbar expand="lg" variant="light" sticky="bottom" bg="light">
            <Form inline className="text-center">
                <Form.Row>
                    <Col md="auto" className="text-center">
                        <Form.Group controlId="formBasicRangeCustom">
                            <Form.Label>Price: {props.filters.min}chf -> {props.filters["Price/m^2"]}chf</Form.Label>
                            <Form.Control type="range" onChange={(e) => {
                                props.changeFilter(e.currentTarget.value, "Price/m^2", null)
                            }} min={props.filters.min} max={props.filters.max} value={props.filters['Price/m^2']}/>
                        </Form.Group></Col>
                    <Col md="auto mt-3" className="justify-content-md-center">

                        {Object.keys(props.filters.buildingType).map((item, i) => (

                            <Form.Check key={i}
                                        custom
                                        inline
                                        checked={props.filters.buildingType[item]}
                                        onChange={(e) => {
                                            props.changeFilter(e.currentTarget.checked, "buildingType", item)
                                        }}
                                        type="checkbox"
                                        id={`checkbox-${item}`}
                                        label={item}

                            />)
                        )}

                    </Col>
                    <Col md="auto mt-3" className="text-center">

                        <Form.Check
                            onChange={(e) => {
                                props.changeFilter(e.currentTarget.checked, "parking", "x")
                            }}
                            type="switch"
                            id="parking"
                            checked={props.filters.parking.x}
                            label="with Parking"
                        />

                    </Col>
                </Form.Row>
            </Form>

        </Navbar>
    }
}