import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as appActions from "./actions/appActions";
import Container from "react-bootstrap/Container";
import Spinner from "react-bootstrap/Spinner"
import FilterComponent from "./components/FilterComponent"
import {MapComponent} from "./components/MapComponent";
import {fetchCsv} from './utils/index';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.changeFilter = this.changeFilter.bind(this);
  }
 componentDidMount() {
	 const getData = async() => {
            const dataReceived = await fetchCsv();
            this.props.appActions.fetchProperties(dataReceived);
        }

        getData();
  }
  changeFilter (e, f, sc){
        this.props.appActions.changeFilters(this.props.filters, e, f, sc)
    }
  render() {
        if (this.props.loading) {
        return (
            <div className={"ctr"}>
                <Spinner animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                </Spinner></div>
        )
    } else if (this.props.error) {
        return (
            <div className={"ctr"}>
                :/ there was an error, please,
                reload the page.
            </div>
        )
    } else {
        return (
            <Container>
                <FilterComponent filters={this.props.filters} changeFilter={this.changeFilter}/>
                <MapComponent properties={this.props.properties} filters={this.props.filters}/>
            </Container>
        );
    }
  }
}

function mapStateToProps(state) {
    return {
        properties: state.appRed.properties,
        filters: state.appRed.filters,
        error: state.appRed.error,
        show: state.appRed.show,
        loading: state.appRed.loading,
        modal: state.appRed.modal,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        appActions: bindActionCreators(appActions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
