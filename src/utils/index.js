import Papa from "papaparse";

const folder = '/data/properties_f.csv';

const parseArray = (arr) => {

    const result = arr.map((x, i) => {
        const arrObj = {};
        if (i === 0) return null;
        for (let n = 0; n < x.length; n++) {
            let text = x[n];
            if (n === 0) {
                text = [];
                const points = x[n].replace(/\(|\)|POINT/ig, "").split(" ");
                points.forEach((k) => {
                    text.push(parseFloat(k));
                })
            }
            arrObj[arr[0][n]] = text;
        }
        return arrObj;
    })
    result.splice(0, 1)
    return result;
}
export const fetchCsv = async () => {
    const response = await fetch(folder);
    const reader = response.body.getReader();
    const result = await reader.read();
    const decoder = new TextDecoder('utf-8');
    const csv = await decoder.decode(result.value);
    const data = Papa.parse(csv);
    const parsed = await parseArray(data.data);
    return parsed;
}
