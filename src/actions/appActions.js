import {
    IS_ERROR,
    IS_LOADING,
    SET_FILTERS,
    RECEIVE_PROPERTIES
} from "./allActions";


export function receiveProperties(data) {
    return {
        type: RECEIVE_PROPERTIES,
        data
    };
}

export function isError(error) {
    return {
        type: IS_ERROR,
        error
    };
}

export function isLoading(loading) {
    return {
        type: IS_LOADING,
        loading
    };
}

export function setFilters(filters) {
    return {
        type: SET_FILTERS,
        filters
    };
}

export function changeFilters(orig, e, f, sc) {
    const merged = {...orig}
    if (sc === null) {
        merged[f] = e;
    } else {
        merged[f][sc] = e;
    }

    return {
        type: SET_FILTERS,
        filters: merged
    };
}

export function fetchProperties(data) {
    try {
//DYNAMIC FILTER GENERATION
        const buildingType = {};
        const parking = {};
        [...new Set(data.map(item => item.BuildingType))].forEach((t) => {
            buildingType[t] = true;
        });
        [...new Set(data.map(item => item.Parking))].forEach((t) => {
            parking[t] = true;
        });
        const max = parseFloat(data.length > 0 && data.reduce((a, b) => parseFloat(a['Price/m^2']) > parseFloat(b['Price/m^2']) ? a : b)['Price/m^2']) || 0
        const min = parseFloat(data.length > 0 && data.reduce((a, b) => parseFloat(a['Price/m^2']) < parseFloat(b['Price/m^2']) ? a : b)['Price/m^2']) || 0
        const filters = {buildingType, parking, 'Price/m^2': max, max, min}
        return dispatch => {
            dispatch(receiveProperties(data));
            dispatch(setFilters(filters));
            dispatch(isLoading(false));
        }
    } catch (e) {
        console.log(e);
        return dispatch => {
            dispatch(isError(true))
        }
    }
}